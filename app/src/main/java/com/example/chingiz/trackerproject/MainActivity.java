package com.example.chingiz.trackerproject;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothSocket;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.PorterDuff;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraManager;
import android.hardware.usb.UsbDevice;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.serenegiant.usb.CameraDialog;
import com.serenegiant.usb.IFrameCallback;
import com.serenegiant.usb.USBMonitor;
import com.serenegiant.usb.UVCCamera;
import com.serenegiant.usbcameracommon.UVCCameraHandlerMultiSurface;
import com.serenegiant.widget.UVCCameraTextureView;

import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Rect2d;
import org.opencv.core.Scalar;
import org.opencv.engine.OpenCVEngineInterface;
import org.opencv.imgproc.Imgproc;
import org.opencv.osgi.OpenCVInterface;
import org.opencv.tracking.Tracker;

import org.opencv.tracking.TrackerGOTURN;
import org.opencv.tracking.TrackerKCF;
import org.opencv.tracking.TrackerMedianFlow;

import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.concurrent.atomic.AtomicReference;

import static android.content.ContentValues.TAG;


public class MainActivity extends Activity implements CvCameraViewListener2, CameraDialog.CameraDialogParent {
    private static final boolean DEBUG = true;

    //Bluetooth side
    private static final String TAG = "bluetooth1";
    private static final int REQUEST_ENABLE_BT = 1;
    private BluetoothAdapter btAdapter = null;
    private BluetoothSocket btSocket = null;
    private OutputStream outStream = null;

    private CameraBridgeViewBase mOpenCvCameraView;
    private Mat frame;
    private Mat frameColor;
    TrackerKCF tr;
    boolean initialized = false;
    boolean newROI = false;
    boolean moving = false;
    Rect2d rect;

    private double CENTER_X = 0;
    private double THRESHOLD = 128;

    final AtomicReference<Point> RectBox_1stCorner = new AtomicReference<Point>();
    final AtomicReference<Point> corner = new AtomicReference<Point>();

    long millisPrev = 0;
    //long millis = 0;


    /**
     * for accessing USB
     */
    private USBMonitor mUSBMonitor;
    /**
     * Handler to execute camera related methods sequentially on private thread
     */
    private UVCCameraHandlerMultiSurface mCameraHandler;
    /**
     * for camera preview display
     */
    private UVCCameraTextureView mUVCCameraView;
    /**
     * for display resulted images
     */
    protected SurfaceView mResultView;

    private ToggleButton mCameraButton;

    private UVCCamera camera;

    private Surface mPreviewSurface;

    private ImageView img;

    BluetoothHelper bt;

    /**
     * set true if you want to record movie using MediaSurfaceEncoder
     * (writing frame data into Surface camera from MediaCodec
     *  by almost same way as USBCameratest2)
     * set false if you want to record movie using MediaVideoEncoder
     */
    private static final boolean USE_SURFACE_ENCODER = false;

    /**
     * preview resolution(width)
     * if your camera does not support specific resolution and mode,
     * {@link UVCCamera#setPreviewSize(int, int, int)} throw exception
     */
    private static final int PREVIEW_WIDTH = 640;
    /**
     * preview resolution(height)
     * if your camera does not support specific resolution and mode,
     * {@link UVCCamera#setPreviewSize(int, int, int)} throw exception
     */
    private static final int PREVIEW_HEIGHT = 480;
    /**
     * preview mode
     * if your camera does not support specific resolution and mode,
     * {@link UVCCamera#setPreviewSize(int, int, int)} throw exception
     * 0:YUYV, other:MJPEG
     */
    private static final int PREVIEW_MODE = 1;

    protected static final int SETTINGS_HIDE_DELAY_MS = 2500;

    private final USBMonitor.OnDeviceConnectListener mOnDeviceConnectListener
            = new USBMonitor.OnDeviceConnectListener() {

        @Override
        public void onAttach(final UsbDevice device) {
            Toast.makeText(MainActivity.this,
                    "USB_DEVICE_ATTACHED", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onConnect(final UsbDevice device,
                              final USBMonitor.UsbControlBlock ctrlBlock, final boolean createNew) {

            if (DEBUG) Log.v(TAG, "onConnect:");
            Toast.makeText(MainActivity.this,
                    "USB_DEVICE_CONNECTED", Toast.LENGTH_SHORT).show();

            //mCameraHandler.open(ctrlBlock);
            camera.open(ctrlBlock);


            try {
                camera.setPreviewSize(UVCCamera.DEFAULT_PREVIEW_WIDTH, UVCCamera.DEFAULT_PREVIEW_HEIGHT, UVCCamera.FRAME_FORMAT_MJPEG);
            } catch (final IllegalArgumentException e) {
                try {
                    // fallback to YUV mode
                    camera.setPreviewSize(UVCCamera.DEFAULT_PREVIEW_WIDTH, UVCCamera.DEFAULT_PREVIEW_HEIGHT, UVCCamera.DEFAULT_PREVIEW_MODE);
                } catch (final IllegalArgumentException e1) {
                    camera.destroy();
                    return;
                }
            }

            //camera.startPreview();


            //mPreviewSurface = mUVCCameraView.getSurface();
            SurfaceTexture texture = new SurfaceTexture(10);



            //mPreviewSurface = new Surface(new SurfaceTexture());
            if (texture != null) {
                //isActive = true;
                //camera.setPreviewDisplay(mPreviewSurface);
                camera.setPreviewTexture(texture);
                camera.startPreview();

                //isPreview = true;
            }



            final Bitmap bitmap = Bitmap.createBitmap(UVCCamera.DEFAULT_PREVIEW_WIDTH, UVCCamera.DEFAULT_PREVIEW_HEIGHT, Bitmap.Config.RGB_565);




            /*
            final Runnable mUpdateImageTask = new Runnable() {
                @Override
                public void run() {
                    synchronized (bitmap) {
                        img.setImageBitmap(bitmap);
                    }
                }
            };
            */

            camera.setFrameCallback(/*new IFrameCallback() {
                @Override
                public void onFrame(final ByteBuffer frame) {
                    //Toast.makeText(MainActivity.this, "FRAME", Toast.LENGTH_SHORT).show();
                    //Log.w("MSG", "Frame captured " + byteBuffer.array().length);
                    //showToast("123");
                    frame.clear();
                    synchronized (bitmap) {
                        bitmap.copyPixelsFromBuffer(frame);
                        img.setImageBitmap(bitmap);
                    }
                    //img.post(mUpdateImageTask);


                }
            }*/ frameCallback, UVCCamera.PIXEL_FORMAT_RGB565);



            /*
            synchronized (mSync) {
                //mUVCCamera = camera;
            }
            */
            //startPreview();
            //updateItems();



            /*
            camera.open(ctrlBlock);

            camera.setFrameCallback(new IFrameCallback() {
                @Override
                public void onFrame(ByteBuffer byteBuffer) {
                    Toast.makeText(MainActivity.this,
                            "FRAME", Toast.LENGTH_SHORT).show();
                }
            }, UVCCamera.PIXEL_FORMAT_RGB565);
            camera.startPreview();
            */

        }

        /*
        private void updateImage() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    synchronized (bitmap) {
                        img.setImageBitmap(bitmap);
                    }
                }
            });
        }*/


        private void showToast(final String text) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(MainActivity.this, text, Toast.LENGTH_SHORT).show();
                }
            });
        }

        @Override
        public void onDisconnect(final UsbDevice device,
                                 final USBMonitor.UsbControlBlock ctrlBlock) {

            if (DEBUG) Log.v(TAG, "onDisconnect:");

        }
        @Override
        public void onDettach(final UsbDevice device) {
            Toast.makeText(MainActivity.this,
                    "USB_DEVICE_DETACHED", Toast.LENGTH_SHORT).show();
            //Utils.bitmapToMat();
        }

        @Override
        public void onCancel(final UsbDevice device) {

        }
    };

    private int mPreviewSurfaceId;
    private void startPreview() {
        if (DEBUG) Log.v(TAG, "startPreview:");
        //mUVCCameraView.resetFps();
        mCameraHandler.startPreview();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {

                    final SurfaceTexture st = mUVCCameraView.getSurfaceTexture();
                    if (st != null) {
                        final Surface surface = new Surface(st);
                        mPreviewSurfaceId = surface.hashCode();
                        mCameraHandler.addSurface(mPreviewSurfaceId, surface, false);

                    }

                    //mCaptureButton.setVisibility(View.VISIBLE);
                    //startImageProcessor(PREVIEW_WIDTH, PREVIEW_HEIGHT);
                } catch (final Exception e) {
                    Log.w(TAG, e);
                }
            }
        });
        //updateItems();
    }

    private void updateItems() {
        runOnUiThread(mUpdateItemsOnUITask);
    }

    private final Runnable mUpdateItemsOnUITask = new Runnable() {
        @Override
        public void run() {
            if (isFinishing()) return;
            /*
            final int visible_active = isActive() ? View.VISIBLE : View.INVISIBLE;
            mToolsLayout.setVisibility(visible_active);
            mBrightnessButton.setVisibility(
                    checkSupportFlag(UVCCamera.PU_BRIGHTNESS)
                            ? visible_active : View.INVISIBLE);
            mContrastButton.setVisibility(
                    checkSupportFlag(UVCCamera.PU_CONTRAST)
                            ? visible_active : View.INVISIBLE);
                            */
        }
    };

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        bt = new BluetoothHelper(this, BluetoothAdapter.getDefaultAdapter(), "00:18:E5:04:A7:96");
        bt.connect();

        setContentView(R.layout.activity_main);

        img = (ImageView) findViewById(R.id.imageView);

        camera = new UVCCamera();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        //mUVCCameraView = findViewById(R.id.camera_view);

        mCameraButton = (ToggleButton) findViewById(R.id.camera_button);
        mCameraButton.setOnCheckedChangeListener(mOnCheckedChangeListener);

        //mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.view);
        //mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
        //mOpenCvCameraView.setCameraIndex(0);
        //mOpenCvCameraView.setCvCameraViewListener(this);
        OpenCVLoader.initDebug();
        tr = TrackerKCF.create();


        final int ImgYOffset = 0;
        final int ImgXOffset = 0;


        CameraManager manager = (CameraManager) getSystemService(CAMERA_SERVICE);
        try {
            String[] cameraList = manager.getCameraIdList();
            int a = cameraList.length;
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }


        img.setOnTouchListener(new View.OnTouchListener() {
            Matrix inverse = new Matrix();
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                img.getImageMatrix().invert(inverse);
                float[] pts = {
                        event.getX(), event.getY()
                };
                inverse.mapPoints(pts);



                corner.set(new Point(pts[0] - ImgXOffset, pts[1] - ImgYOffset));
                switch(event.getAction()){
                    case MotionEvent.ACTION_DOWN:
                        RectBox_1stCorner.set(corner.get());

                        break;
                    case MotionEvent.ACTION_UP:
                        newROI = true;
                        break;
                    case MotionEvent.ACTION_MOVE:
                        Imgproc.rectangle(frameColor, new Point(RectBox_1stCorner.get().x, RectBox_1stCorner.get().y), new Point(corner.get().x, corner.get().y), new Scalar(255, 0, 0), 2);
                        moving = true;
                        break;
                }

                return true;
            }
        });


        /*mOpenCvCameraView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                corner.set(new Point(event.getX() - ImgXOffset, event.getY() - ImgYOffset));
                switch(event.getAction()){
                    case MotionEvent.ACTION_DOWN:
                        RectBox_1stCorner.set(corner.get());

                        break;
                    case MotionEvent.ACTION_UP:
                        newROI = true;
                        break;
                    case MotionEvent.ACTION_MOVE:
                        Imgproc.rectangle(frameColor, new Point(RectBox_1stCorner.get().x, RectBox_1stCorner.get().y), new Point(corner.get().x, corner.get().y), new Scalar(255, 0, 0), 2);
                        moving = true;
                        break;
                }

                return true;
            }
        });
        */






        mUSBMonitor = new USBMonitor(this, mOnDeviceConnectListener);

        mCameraHandler = UVCCameraHandlerMultiSurface.createHandler(this, mUVCCameraView, USE_SURFACE_ENCODER ? 0 : 1, PREVIEW_WIDTH, PREVIEW_HEIGHT, PREVIEW_MODE);

    }

    @Override
    protected void onStart() {
        super.onStart();
        mUSBMonitor.register();
    }

    @Override
    public void onPause()
    {
        super.onPause();
        if(bt != null && bt.isConnected())
            bt.disconnect();

        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        if(bt != null && !bt.isConnected())
            bt.connect();

        OpenCVLoader.initDebug();
        //mOpenCvCameraView.enableView();
    }

    public void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();


        if (mUSBMonitor != null) {
            mUSBMonitor.destroy();
            mUSBMonitor = null;
        }
    }

    public void onCameraViewStarted(int width, int height) {
    }

    public void onCameraViewStopped() {
    }

    IFrameCallback frameCallback = new IFrameCallback() {
        @Override
        public void onFrame(final ByteBuffer inputFrame) {
            inputFrame.clear();
            Bitmap bm = Bitmap.createBitmap(UVCCamera.DEFAULT_PREVIEW_WIDTH, UVCCamera.DEFAULT_PREVIEW_HEIGHT, Bitmap.Config.RGB_565);
            bm.copyPixelsFromBuffer(inputFrame);
            frame = new Mat();
            frameColor = new Mat();
            Utils.bitmapToMat(bm, frameColor);
            Utils.bitmapToMat(bm, frame);
            Imgproc.cvtColor(frame, frame, Imgproc.COLOR_RGB2GRAY);

            CENTER_X = frame.width() / 2;

            if(RectBox_1stCorner.get() != null && corner != null && moving){
                Imgproc.rectangle(frameColor, new Point(RectBox_1stCorner.get().x, RectBox_1stCorner.get().y), new Point(corner.get().x, corner.get().y), new Scalar(255, 0, 0), 2);
                moving = false;
            }

            if(!initialized){

                if(RectBox_1stCorner.get() != null && corner != null){
                    rect = new Rect2d(RectBox_1stCorner.get(), corner.get());
                    tr.init(frame, rect);
                    initialized = true;

                }

            } else {

                boolean ok;
                if(newROI){
                    //ok = tr.update(frame, new Rect2d(RectBox_1stCorner.get(), corner.get()));
                    tr.clear();
                    tr = TrackerKCF.create();
                    ok = tr.init(frame, new Rect2d(RectBox_1stCorner.get(), corner.get()));
                    newROI = false;
                } else {
                    ok = tr.update(frame, rect);
                }



                if (ok) {
                    Point p1 = new Point(rect.x, rect.y);
                    Point p2 = new Point(rect.x + rect.width, rect.y + rect.height);

                    Imgproc.rectangle(frameColor, p1, p2, new Scalar(255, 0, 0), 2);

                    if(rect.x + (rect.width / 2) < CENTER_X - THRESHOLD){
                        Imgproc.putText(frameColor, "ON LEFT", new Point(100, 120), Core.FONT_HERSHEY_SIMPLEX, 0.75, new Scalar(0, 0, 255), 2);

                        if(System.currentTimeMillis() - millisPrev > 200){
                            bt.sendData(0);
                            millisPrev = System.currentTimeMillis();
                        }


                    } else if(rect.x + (rect.width / 2) > CENTER_X + THRESHOLD){
                        Imgproc.putText(frameColor, "ON RIGHT", new Point(100, 120), Core.FONT_HERSHEY_SIMPLEX, 0.75, new Scalar(0, 0, 255), 2);
                        if(System.currentTimeMillis() - millisPrev > 200){
                            bt.sendData(1);
                            millisPrev = System.currentTimeMillis();
                        }
                    } else {
                        Imgproc.putText(frameColor, "CENTERED", new Point(100, 120), Core.FONT_HERSHEY_SIMPLEX, 0.75, new Scalar(0, 0, 255), 2);
                    }

                } else {
                    Imgproc.putText(frameColor, "Tracking failure", new Point(100, 80), Core.FONT_HERSHEY_SIMPLEX, 0.75, new Scalar(0, 0, 255), 2);
                }

                Imgproc.putText(frameColor, "KCF TRACKER", new Point(100, 20), Core.FONT_HERSHEY_SIMPLEX, 0.75, new Scalar(50, 170, 50), 2);

            }

            Bitmap resultBitmap = Bitmap.createBitmap(frameColor.cols(),  frameColor.rows(), Bitmap.Config.RGB_565);;
            Utils.matToBitmap(frameColor, resultBitmap);

            synchronized (img){
                if(resultBitmap != null)
                    img.setImageBitmap(resultBitmap);
            }



        }
    };

    public Mat onCameraFrame(CvCameraViewFrame inputFrame) {



        Mat inp = inputFrame.gray();
        //Mat m = new Mat()
        frameColor = inputFrame.rgba();
        frame = inp;
        CENTER_X = frame.width() / 2;

        if(RectBox_1stCorner.get() != null && corner != null && moving){
            Imgproc.rectangle(frameColor, new Point(RectBox_1stCorner.get().x, RectBox_1stCorner.get().y), new Point(corner.get().x, corner.get().y), new Scalar(255, 0, 0), 2);
            moving = false;
        }

        if(!initialized){

            if(RectBox_1stCorner.get() != null && corner != null){
                rect = new Rect2d(RectBox_1stCorner.get(), corner.get());
                tr.init(frame, rect);
                initialized = true;

            }

        } else {

            boolean ok;
            if(newROI){
                //ok = tr.update(frame, new Rect2d(RectBox_1stCorner.get(), corner.get()));
                tr.clear();
                tr = TrackerKCF.create();
                ok = tr.init(frame, new Rect2d(RectBox_1stCorner.get(), corner.get()));
                newROI = false;
            } else {
                ok = tr.update(frame, rect);
            }



            if (ok) {
                Point p1 = new Point(rect.x, rect.y);
                Point p2 = new Point(rect.x + rect.width, rect.y + rect.height);

                Imgproc.rectangle(frameColor, p1, p2, new Scalar(255, 0, 0), 2);

                if(rect.x + (rect.width / 2) < CENTER_X - THRESHOLD){
                    Imgproc.putText(frameColor, "ON LEFT", new Point(100, 120), Core.FONT_HERSHEY_SIMPLEX, 0.75, new Scalar(0, 0, 255), 2);
                } else if(rect.x + (rect.width / 2) > CENTER_X + THRESHOLD){
                    Imgproc.putText(frameColor, "ON RIGHT", new Point(100, 120), Core.FONT_HERSHEY_SIMPLEX, 0.75, new Scalar(0, 0, 255), 2);
                } else {
                    Imgproc.putText(frameColor, "CENTERED", new Point(100, 120), Core.FONT_HERSHEY_SIMPLEX, 0.75, new Scalar(0, 0, 255), 2);
                }

            } else {
                Imgproc.putText(frameColor, "Tracking failure", new Point(100, 80), Core.FONT_HERSHEY_SIMPLEX, 0.75, new Scalar(0, 0, 255), 2);
            }

            Imgproc.putText(frameColor, "KCF TRACKER", new Point(100, 20), Core.FONT_HERSHEY_SIMPLEX, 0.75, new Scalar(50, 170, 50), 2);

        }

        return frameColor;

    }

    @Override
    public USBMonitor getUSBMonitor() {
        return mUSBMonitor;
    }

    @Override
    public void onDialogResult(boolean canceled) {
        if (DEBUG) Log.v(TAG, "onDialogResult:canceled=" + canceled);
        if (canceled) {
            setCameraButton(false);
        }
    }

    private final CompoundButton.OnCheckedChangeListener mOnCheckedChangeListener
            = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(
                final CompoundButton compoundButton, final boolean isChecked) {

            switch (compoundButton.getId()) {
                case R.id.camera_button:
                    if (isChecked /*&& !mCameraHandler.isOpened()*/) {
                        CameraDialog.showDialog(MainActivity.this);
                    } else {
                        //stopPreview();
                    }
                    break;
            }
        }
    };

    private void setCameraButton(final boolean isOn) {
        if (DEBUG) Log.v(TAG, "setCameraButton:isOn=" + isOn);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mCameraButton != null) {
                    try {
                        mCameraButton.setOnCheckedChangeListener(null);
                        mCameraButton.setChecked(isOn);
                    } finally {
                        mCameraButton.setOnCheckedChangeListener(mOnCheckedChangeListener);
                    }
                }
                /*
                if (!isOn && (mCaptureButton != null)) {
                    mCaptureButton.setVisibility(View.INVISIBLE);
                }
                */
            }
        });
        updateItems();
    }


}